// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    // Download the helper library from https://www.twilio.com/docs/node/install
    // Your Account Sid and Auth Token from twilio.com/console
    // DANGER! This is insecure. See http://twil.io/secure
    const accountSid = 'AC5ecb498b16f1dbb1823571c91644d869';
    const authToken = '2f5aa26ba59822de8a4effe3d45402df';
    const client = require('twilio')(accountSid, authToken);
    
    const { result } = context;
    
    console.log( "Sms sent: " + result['to']);
    
    client.messages
          .create({body: 'Hi there!', from: '+16046700949', to: result['to']})
          .then(message => console.log(message.sid));
          
    return context;
  };
}
